package com.generalui.shysampleandroid;

import android.app.Application;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;
import android.support.test.uiautomator.UiObject2;
import android.test.ApplicationTestCase;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static org.junit.Assert.assertThat;

/**
 * <a href="http://d.android.com/tools/testing/testing_android.html">Testing Fundamentals</a>
 */
@RunWith(AndroidJUnit4.class)
public class ApplicationTest extends ApplicationTestCase<Application> {
    public ApplicationTest() {
        super(Application.class);
    }

    @Rule
    public ActivityTestRule<MyActivity> activityTestRule = new ActivityTestRule<MyActivity>(MyActivity.class);

    @Test
    public void testMyActiivty() {
        MyActivityPage myActivityPage = new MyActivityPage();

        myActivityPage.enterText("Test");

        myActivityPage.clickSend();

        ResultPage result = new ResultPage();
        assertTrue("Unable to find text on result page", result.verifyText("Test"));
    }

}