package com.generalui.shysampleandroid;

import android.app.Instrumentation;
import android.support.test.InstrumentationRegistry;
import android.support.test.uiautomator.By;
import android.support.test.uiautomator.UiDevice;
import android.support.test.uiautomator.UiObject2;
import android.support.test.uiautomator.Until;

/**
 *
 * Created by jenkins on 12/14/15.
 */
public class MyActivityPage {
    private static final String LOG_TAG = MyActivityPage.class.getSimpleName();
    private static final long ACTION_TIMEOUT = 5000;

    UiDevice device = UiDevice.getInstance(InstrumentationRegistry.getInstrumentation());
    Instrumentation instrumentation = InstrumentationRegistry.getInstrumentation();

    private enum Items {
    }

    /**
     *
     * @param text
     * @return
     */
    public boolean enterText(final String text) {
        device.waitForIdle();
        device.wait(Until.findObject(
                By.res("com.generalui.shysampleandroid:id/edit_message")), ACTION_TIMEOUT);

        UiObject2 editText = device.findObject(
                By.res("com.generalui.shysampleandroid:id/edit_message"));

        boolean result = editText != null;
        editText.setText(text);

        return result;
    }

    public void clickSend() {
        UiObject2 sendBtn = device.findObject(By.res("com.generalui.shysampleandroid:id/button_send"));

        sendBtn.clickAndWait(Until.newWindow(), ACTION_TIMEOUT);
    }

}
