package com.generalui.shysampleandroid;

import android.support.test.InstrumentationRegistry;
import android.support.test.uiautomator.By;
import android.support.test.uiautomator.UiDevice;
import android.support.test.uiautomator.UiObject;
import android.support.test.uiautomator.UiObject2;

/**
 * Created by jenkins on 12/14/15.
 */
public class ResultPage {
    UiDevice device = UiDevice.getInstance(InstrumentationRegistry.getInstrumentation());

    public boolean verifyText(final String target) {
        UiObject2 targetText = device.findObject(By.text(target));

        return targetText != null;
    }
}
